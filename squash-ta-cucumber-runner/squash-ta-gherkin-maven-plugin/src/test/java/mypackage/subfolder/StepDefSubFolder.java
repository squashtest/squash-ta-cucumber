/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
 package mypackage.subfolder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefSubFolder {

	@Given("first arg is <firstNumber>")
	public void ThrowButnotPendingException() {
	    // Write code here that turns the phrase above into concrete actions
		new cucumber.api.PendingException();
	    throw new NumberFormatException("I am not PendingException");
	}

	@Given("second arg is <secondNumber>")
	public void second_arg_is_secondNumber() {
	}

	@Then("complexe operation gives {int}")
	public void complexe_operation_gives(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@Given("firstArg is {int}")
	public void firstarg_is(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@Given("secondArg is {int}")
	public void secondarg_is(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("I am doing something..");
	}

	@Then("divide should be {int}")
	public void divide_should_be(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new cucumber.api.PendingException();
	}
	
	
	// T8 
	
//	@Given("given first step line {int}") //not implemented
//	public void given_first_step_line(Integer int1) {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new cucumber.api.PendingException();
//	}

	@Given("{int}d given step line {int}") //empty
	public void d_given_step_line(Integer int1, Integer int2) {
	    // Write code here that turns the phrase above into concrete actions
//	    throw new cucumber.api.PendingException();
//		int i=3;
	}

	@When("step when line {int}") //exist and not empty
	public void step_when_line(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
	   int i=2;
	}

	@Then("step Then line {int}") // empty
	public void step_Then_line(Integer int1) {
	    // Write code here that turns the phrase above into concrete actions
//	    throw new cucumber.api.PendingException();
//		int i=4;
	}

}
