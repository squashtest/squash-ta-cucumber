/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package mypackage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import data.IsItFriday;
import cucumber.api.java.en.Then;
import static org.junit.Assert.*;

import java.util.List;



public class StepdefsIsItFriday {
    private String today;
    private String actualAnswer;
    static int index = 0;

    @Given("^today is \"([^\"]*)\"$")
    public void today_is(String today) {
        this.today = today;
    }

    @When("^I ask whether it's Friday yet$")
    public void i_ask_whether_is_s_Friday_yet() {
        this.actualAnswer = IsItFriday.isItFriday(today);
        
    }
    
    @When("^I ask whether it's Friday yet bis$")
    public void divde_by_zero_on_second_call() {
    	index++;
    	if (index == 2) { //divide by zero on second dataset
    		int y = 4/0;
    	}
    	if (index == 5) { //nullPointerException on 5th Dataset
    		List<String> liste = null;
    		liste.add("peuImporte ....");
    		
    	}
        this.actualAnswer = IsItFriday.isItFriday(today);
        
    }
    
    @When("^I ask whether it's Friday yet2$")
    public void i_ask_whether_is_s_Friday_yet2() {
        this.actualAnswer = IsItFriday.isItFriday(today);
        
    }
    
    @When("Oops Java Method exists but is not implemented so raising exception")
    public void oops_Java_Method_exists_but_is_not_implemented_so_raising_exception() {
        throw new cucumber.api.PendingException();
    //	throw new mypackage.MyException();
    }

    @Then("^I should be told \"([^\"]*)\"$")
    public void i_should_be_told(String expectedAnswer) {
        assertEquals(expectedAnswer, actualAnswer);
    }
   
}
