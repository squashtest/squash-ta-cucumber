/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package mypackage;

import java.io.File;

import cucumber.api.java.en.Then;

public class StepsWithRunTimeException {
	
	@Then("God Divide by zero Exception")
	public void god_Divide_by_zero_Exception() {
		int i = 1/0;
	}
	
	@Then("NullPOinterException Raising")
	public void nullpointerexception_Raising() {
	   File file =null;
	   file.canRead();
	   
	}
}