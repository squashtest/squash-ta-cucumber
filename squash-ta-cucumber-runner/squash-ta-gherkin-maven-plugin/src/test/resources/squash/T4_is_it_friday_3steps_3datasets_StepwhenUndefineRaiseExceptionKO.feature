 Feature: T4: Is it Friday Yet?
  1Step exists but raises an Not Implemented Exception

  Scenario Outline: Today is or is not Friday
    Given today is "<day>"
    When Oops Java Method exists but is not implemented so raising exception
    Then I should be told "<answer>"

  Examples:
    | day | answer |
    | Friday | TGIF |
    | Sunday | Nope |
    | anything else! | Nope |