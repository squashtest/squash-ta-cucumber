 Feature: T7 complexeOperation: ?
  ...not important

  @WithDataset
  Scenario Outline: complexe calc
    Given first arg is <firstNumber>
    Given second arg is <secondNumber>
    Then complexe operation gives <result>

  Examples:
    | arg1 | arg2 | result | 
    |1|1|2|
    |2|2|6|
    |2|3|5|
    
    @WithoutDataset @Manual
    Scenario: divide numbers
    Given firstArg is 10
    Given secondArg is 5
    Then divide should be 2
     Given secondArg is 7
     Given secondArg is 15
     Given firstArg is 10
    
  Scenario: I am a clone of T1
    Given today is "Friday"
    When I ask whether it's Friday yet
    Then I should be told "TGIF"