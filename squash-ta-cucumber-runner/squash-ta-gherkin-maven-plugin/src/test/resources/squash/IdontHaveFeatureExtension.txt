Feature: File with no feature extension
  This file does not to be excecuted

  Scenario Outline: Today is or is not Friday
    Given today is "<day>"
    When I ask whether it's Friday yet
    Then I should be told "<answer>"

  Examples:
    | day | answer | 
    | Friday | TGIF |
    | Sunday | Nope |
    | anything else! | Nope |