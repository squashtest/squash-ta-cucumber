 Feature: T5: Is it Friday Yet?
  Step3 raises an Exception for Divide BY 0 (1 dataset)

  Scenario: Today is or is not Friday
    Given today is "Friday"
    Then God Divide by zero Exception
    When I ask whether it's Friday yet
    Then God Divide by zero Exception
    
Scenario Outline: Today is or suite
    Given today is "<day>"
    When I ask whether it's Friday yet
    Then I should be told "<answer>"
    
    Examples:
     | day | answer |
    | Friday | TGIF |
    | Sunday | Nospe |
    