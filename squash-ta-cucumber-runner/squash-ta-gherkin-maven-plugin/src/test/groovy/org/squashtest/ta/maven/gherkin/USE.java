/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.codehaus.plexus.util.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.springframework.util.StringUtils;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.gherkin.exporter.HtmlGherkinSuiteResultExporter;

/**
 *
 * @author cjuillard
 */
public class USE {

    private static Document report;
    public static String body;

    private USE() {
    }

    public static void setHTMLReport(File htmlReport) throws IOException {
        assertTrue(htmlReport.exists());
        assertTrue(htmlReport.isFile());
        report = Jsoup.parse(htmlReport, "UTF-8");
        body = report.body().text();
        body = body.toLowerCase();
    }

    public static int findString(String data) throws IOException {
        data = data.toLowerCase();
        return body.indexOf(data);
    }

    public static int findString(String data, int begin) throws IOException {
        data = data.toLowerCase();
        return body.indexOf(data, begin);
    }

    public static int countOccurrencesOf(String txt, int start, int end) {
        String subbody = body.substring(start, end);
        return StringUtils.countOccurrencesOf(subbody, txt.toLowerCase());
    }

    public static void chekHTMLReport(List<String> expectedTxts) throws IOException {
        //exception si report=null;
        for (String txt : expectedTxts) {
            assertTrue("Expected in html: '" + txt + "'", body.contains(txt.toLowerCase()));
        }
    }

    public static void chekHTMLReport(List<String> expectedTxts, int start, int end) throws IOException {
        String subbodyWithCarriageReturn = body.substring(start, end);
        String subbody = subbodyWithCarriageReturn.replaceAll("[\n\r] ", "");
        for (String txt : expectedTxts) {
            assertTrue("Expected in html: '" + txt + "'", subbody.contains(txt.toLowerCase()));
        }
    }

    public static void chekNotExistInHTMLReport(String notExpectedTxt, int start, int end) {
        String subbody = body.substring(start, end);
        assertFalse("Unexpected string (should not be in html report): '" + notExpectedTxt + "'", subbody.contains(notExpectedTxt.toLowerCase()));
    }

    public static void setGherkinHTMLExporter(SquashTaGherkinTestExecuteMojo mojoRun) {
        ResultExporter[] gkexporters = new ResultExporter[1];
        ResultExporter exporter = new HtmlGherkinSuiteResultExporter();
        mojoRun.gkexporters = gkexporters;
        mojoRun.isGherkinHTLMExporter = true;
        gkexporters[0] = exporter;
    }

    /**
     *
     * @param suiteResult SuiteREsult to check
     * @param totalTests totalTests in suite
     * @param totalError total test in error
     * @param totalFailure total test in failre
     * @param totalNotFound total test not found
     * @param totalNotRun total test not passed
     * @param totalSuccess total test success
     */
    public static void checkResult(SuiteResult suiteResult, int totalTests,
            int totalSuccess, int totalFailure, int totalError,
            int totalNotFound, int totalNotRun) {
        Assert.assertEquals("totalTests", suiteResult.getTotalTests(), totalTests);
        Assert.assertEquals("successes", suiteResult.getTotalSuccess(), totalSuccess);

        Assert.assertEquals("failures", suiteResult.getTotalFailures(), totalFailure);
        Assert.assertEquals("errors", suiteResult.getTotalErrors(), totalError);
        Assert.assertEquals("not found", suiteResult.getTotalNotFound(), totalNotFound);
        Assert.assertEquals("not run", suiteResult.getTotalNotRun(), totalNotRun);
    }

    public static void clean(File projectBaseDirectory) {
        File html = new File(projectBaseDirectory, "html-reports");
        File surfire = new File(projectBaseDirectory, "surefire-reports");
        if (surfire.exists() && surfire.isDirectory()) {
            try {
                FileUtils.deleteDirectory(surfire);
            } catch (IOException ex) {
                //LOGGER.warn("error in cleaning test resource: " + surfire, ex);
            }
        }

        if (html.exists() && html.isDirectory()) {
            try {
                FileUtils.deleteDirectory(html);
            } catch (IOException ex) {
                //LOGGER.warn("error in cleaning test resource: " + html, ex);
            }
        }
    }
}
