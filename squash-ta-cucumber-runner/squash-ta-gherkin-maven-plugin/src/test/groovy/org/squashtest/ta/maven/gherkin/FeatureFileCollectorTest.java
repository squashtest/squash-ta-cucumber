/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.gherkin.exploitation.testworkspace.FeatureFileCollector;

/**
 *
 * @author qtran
 */
public class FeatureFileCollectorTest extends ResourceExtractorTestBase{
    private FeatureFileCollector testee = new FeatureFileCollector();
    
    private File featureDir;
    
    @Before
    public void createFileTree() throws IOException {
        featureDir=createNtrackDir();
        touchFeatureFile(featureDir,"feature-in-root.feature");
        File subFolder = new File(featureDir,"sub-folder");
        subFolder.mkdirs();
        touchFeatureFile(subFolder, "feature-in-sub-folder.feature");
        touchFeatureFile(featureDir,"non-feature-in-root.txt");
    }

    private void touchFeatureFile(File directory, String name) throws IOException {
        new File(directory, name).createNewFile();
    }
    
    @Test
    public void getFeatureFilesTest(){
        List<File>features = testee.getFeatureFiles(featureDir.getAbsolutePath());
        Assert.assertTrue(features.size() == 2);
    }
    
}
