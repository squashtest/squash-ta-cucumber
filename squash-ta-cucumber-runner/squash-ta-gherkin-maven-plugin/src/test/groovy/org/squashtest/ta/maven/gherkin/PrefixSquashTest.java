/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.maven.plugin.MojoExecutionException;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.commons.factories.specification.TestSpecification;
import org.squashtest.ta.plugin.cucumber.library.Conf;

/**
 *
 * @author cjuillard
 */
public class PrefixSquashTest {

    SquashTAGherkinTestRunableMojo mojoDryrun = new SquashTAGherkinTestRunableMojo();
    static File projectBaseDirectory;
    static File featureBase;
    static SuiteSpecification suite;

    @BeforeClass
    public static void set() {
        projectBaseDirectory = new File(System.getProperty("user.dir"));
        assertTrue(projectBaseDirectory.exists());
        assertTrue(projectBaseDirectory.isDirectory());

        featureBase = new File(projectBaseDirectory, "src/test/resources");
        assertTrue(featureBase.exists());
        assertTrue(featureBase.isDirectory());
    }

    @Test
    public void ctrlSquashRootNotJSONTest() throws MojoExecutionException {
        //set to mojo
        mojoDryrun.featureFileDirectory = featureBase;
        mojoDryrun.setProjectBaseDirectory(projectBaseDirectory);
        //squashRoot must be ignored
        mojoDryrun.squashRoot = "";
        List<String> liste = mojoDryrun.buildExistingFeaturesList(featureBase.getAbsolutePath());
        assertTrue(liste.contains("operations/SimpleOperations.feature"));
        assertTrue(liste.contains("squash/AnotherMalformedFeature.feature"));
        verifyListFeatureFileExit(liste);

        mojoDryrun.squashRoot = "squash";
        liste = mojoDryrun.buildExistingFeaturesList(featureBase.getAbsolutePath());
        assertTrue(liste.contains("operations/SimpleOperations.feature"));
        assertTrue(liste.contains("squash/AnotherMalformedFeature.feature"));
        verifyListFeatureFileExit(liste);

    }

    @Test
    public void ctrlSquashRootWithJSONTest() throws MojoExecutionException {
        //set to mojo
        mojoDryrun.featureFileDirectory = featureBase;
        mojoDryrun.setProjectBaseDirectory(projectBaseDirectory);
        //no needs to add a prefix for this JSON test list
        File json = new File(projectBaseDirectory, "src/test/resources/JSON_TestsToExecute/Testsuite.json");
        assertTrue(json.exists());
        assertFalse(json.isDirectory());

        String paramJSON = "{file:" + json.getAbsolutePath() + "}";
        mojoDryrun.squashRoot = "";
        suite = mojoDryrun.convertoTestSuite(paramJSON);
        assertTrue(isTestInList(suite, "/operations/SimpleOperations.feature"));
        assertTrue(isTestInList(suite, "/squash/AnotherMalformedFeature.feature"));

        //JSON for which TF must add the prefix squashRoot before builing SuiteSpec.
        json = new File(projectBaseDirectory, "src/test/resources/JSON_TestsToExecute/SquashFeaturesWithoutPrefix.json");
        paramJSON = "{file:" + json.getAbsolutePath() + "}";
        suite = mojoDryrun.convertoTestSuite(paramJSON);
        //bad squashRoot definition 
        assertTrue(isTestInList(suite, "/operations/SimpleOperations.feature"));
        assertFalse(isTestInList(suite, "/squash/AnotherMalformedFeature.feature"));

        mojoDryrun.squashRoot = "squash";
        suite = mojoDryrun.convertoTestSuite(paramJSON);
        assertFalse(isTestInList(suite, "operations/SimpleOperations.feature"));
        assertTrue(isTestInList(suite, mojoDryrun.squashRoot + "/" + "operations/SimpleOperations.feature"));
        assertFalse(isTestInList(suite, "AnotherMalformedFeature.feature"));
        assertTrue(isTestInList(suite, mojoDryrun.squashRoot + "/" + "AnotherMalformedFeature.feature"));
    }

    /**
     * *****************************************************************
     */
    /**
     * *************** NOT TEST METHODS ****************************
     */
    /**
     * *****************************************************************
     */
    private void verifyListFeatureFileExit(List<String> liste) {
        for (String feature : liste) {
            verifyFileExit(feature);
            assertTrue("Bad extension", feature.toLowerCase().endsWith(Conf.FEATURE_EXTENTSION));
        }
    }

    private void verifyFileExit(String name) {
        File file = new File(featureBase, name);
        assertTrue("checking " + name, file.exists());
        assertFalse("checking " + name, file.isDirectory());
    }

    private boolean isTestInList(SuiteSpecification suite, String test) {
        List<TestSpecification> tests = suite.getTests();
        List<TestSpecification> foundTest = tests.stream().filter(s -> s.getScript().equals(test)).collect(Collectors.toList());
        assertTrue("SuiteSpecification contains duplicate tests", foundTest.size() <= 1);
        return (foundTest.isEmpty() ? false : true);
    }
}
