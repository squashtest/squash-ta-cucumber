/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.plugin.MojoExecutionException;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.squashtest.ta.backbone.init.EngineLoader;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.core.tools.io.TempFileUtils;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.gherkin.exploitation.factory.GherkinTMModeTestSuiteFactory;

/**
 *
 * @author cjuillard
 */
public class ExecuteTestSuiteTest {

    private File projectBaseDirectory;
    private File featureBase;
    private List<String> expectedTxts = new ArrayList<>();
    SquashTaGherkinTestExecuteMojo mojoRun;
    private final String backFeatureList = "[Back to top] [Back to FeatureList]";
    private final int delta = "[Back to top] [Back to FeatureList]".length();
    private int start;
    private int end;

    @Before
    public void setup() throws IOException {
        TempFileUtils.init(null, null);
        projectBaseDirectory = new File(System.getProperty("user.dir"));
        mojoRun = new SquashTaGherkinTestExecuteMojo();
        mojoRun.squashRoot = null;
        USE.setGherkinHTMLExporter(mojoRun);
        mojoRun.isDryRun = false;
        mojoRun.projectBaseDirectory = projectBaseDirectory;
        mojoRun.featureFileDirectory = featureBase;
        mojoRun.buildDirectory = new File(projectBaseDirectory, "squashTA");

        mojoRun.projectArtifactId = "squash-ta-gherkin-maven-plugin";
        mojoRun.projectGroupId = "GroupID...";
        mojoRun.projectVersion = "ProjectVersion...";

        mojoRun.testDirectory = new File(projectBaseDirectory, "src/test").getAbsolutePath();
        mojoRun.buildDirectory = new File(projectBaseDirectory, "target");
        mojoRun.featureFileDirectory = new File(projectBaseDirectory, "src/test/resources");

        mojoRun.outputDirectory = new File(projectBaseDirectory, "target/squashTF");
        if (!mojoRun.outputDirectory.exists()) {
            mojoRun.outputDirectory.mkdir();
        }
        assertTrue(mojoRun.outputDirectory.exists());
        assertTrue(mojoRun.outputDirectory.isDirectory());
    }

    @Test
    public void executeTestList() throws IOException, MojoExecutionException {

        File json = new File(projectBaseDirectory, "src/test/resources/JSON_TestsToExecute/Testsuite.json");
        assertTrue(json.exists());
        assertFalse(json.isDirectory());
        mojoRun.featuresList = "{file:" + json.getAbsolutePath() + "}";

        SuiteSpecification specification = mojoRun.convertoTestSuite(mojoRun.featuresList);
        GherkinTMModeTestSuiteFactory factory = new GherkinTMModeTestSuiteFactory(mojoRun.projectArtifactId + "/src/test", false, false, true);

        TestSuite suite = factory.generateTestSuite(mojoRun.projectGroupId, mojoRun.projectArtifactId, mojoRun.projectVersion, specification.getTests());
        TestWorkspaceBrowser browser = mojoRun.makeWorkspaceBrowser();
        EngineLoader loader = new EngineLoader();
        Engine engine = loader.load();
        SuiteResult suiteResult = /*(DefaultSuiteResult)*/ engine.execute(suite, browser);
        Assert.assertEquals(suiteResult.getName(),"GherkinExecution");
        mojoRun.exportResults(suiteResult);

        //DefaultSUiteResult
        USE.checkResult(suiteResult, 16, 1, 2, 12, 1, 0);

        //GherkinSUiteResult
        File htmlReport = new File(projectBaseDirectory, "html-reports/squash-tf-report.html");
        USE.setHTMLReport(htmlReport);
        //titre
        expectedTxts.clear();
        expectedTxts.add("Automated Test Execution Report Gherkin Tests - Goal: Run");
        expectedTxts.add("ArtifactId: squash-ta-gherkin-maven-plugin");
        expectedTxts.add("GroupId: GroupID...");
        USE.chekHTMLReport(expectedTxts);
        //bilan ecosystem
        int endEcosysTable = USE.findString("[Back to top]", USE.findString("EmptyFile.feature Error")) + "[Back to top]".length();
        expectedTxts.clear();
        expectedTxts.add("squash/T1_is_it_friday_3steps_3datasets.feature Success");
        expectedTxts.add("squash/T2_is_it_friday_3steps_3datasets_dataset3KO.feature Failure");
        expectedTxts.add("squash/T3_is_it_friday_3steps_3datasets_NoMatchingMethodStepWhen.feature Error");
        expectedTxts.add("squash/IdontExist.feature Not found squash/IdontHaveFeatureExtension.txt Error");
        expectedTxts.add("squash/T4_is_it_friday_3steps_3datasets_StepwhenUndefineRaiseExceptionKO.feature Error");
        expectedTxts.add("squash/T5_is_it_friday_3steps_Exception_divideBy0.feature Error");
        expectedTxts.add("operations/SimpleOperations.feature Failure");
        expectedTxts.add("squash/MalformedFeature.feature Error");
        expectedTxts.add("squash/T6_NullPointerException.feature Error");
        expectedTxts.add("squash/AnotherMalformedFeature.feature Error");
        expectedTxts.add("squash/subfolder/T7ComplexeOperations.feature Error");
        expectedTxts.add("squash/subfolder/T8NotImplementedStepsAndEmptyImplementation.feature Error");
        expectedTxts.add("squash/T2_BISis_it_friday_3steps_3datasets_dataset1failurepuis runtimerror.feature Error");
        expectedTxts.add("squash/ScenarioOutlineWithOutdataSet.feature Error");
        expectedTxts.add("squash/EmptyFile.feature Error");
        USE.chekHTMLReport(expectedTxts, 0, endEcosysTable);
        //T1 OK
        this.start = endEcosysTable;
        this.end = USE.findString(backFeatureList, endEcosysTable) + delta;
        expectedTxts.clear();
        expectedTxts.add("squash/T1_is_it_friday_3steps_3datasets.feature");
        expectedTxts.add("Status: Success");
        expectedTxts.add("Scenario Outline -> DataSet: 1/3");
        expectedTxts.add("Scenario Outline -> DataSet: 2/3");
        expectedTxts.add("Scenario Outline -> DataSet: 3/3");
        expectedTxts.add("Given today is \"Friday\"");
        expectedTxts.add("matching java method: StepdefsIsItFriday.today_is(String)");
        expectedTxts.add("Attachments");
        expectedTxts.add("EXECUTION_REPORT-cucumber.json");
        USE.chekHTMLReport(expectedTxts, start, end);
        //T2 failure 
        moveStartEnd();
        expectedTxts.clear();
        expectedTxts.add("squash/T2_is_it_friday_3steps_3datasets_dataset3KO.feature");
        expectedTxts.add("Scenario Outline -> DataSet: 1/4");
        expectedTxts.add("Scenario Outline -> DataSet: 4/4");
        expectedTxts.add("Then I should be told \"TGsIF\"");
        expectedTxts.add("matching java method: StepdefsIsItFriday.i_should_be_told(String)");
        expectedTxts.add("org.junit.ComparisonFailure: expected:<TG[s]IF> but was:<TG[]IF>");
        expectedTxts.add("Attachments");
        expectedTxts.add("EXECUTION_REPORT-cucumber.json");
        USE.chekHTMLReport(expectedTxts, start, end);
        //T3 error 
        moveStartEnd();
        expectedTxts.clear();
        expectedTxts.add("When no existing(matching) method for this step matching java method: No match!");
        Assert.assertTrue(USE.countOccurrencesOf("matching java method: No match!", start, end) == 3);
        expectedTxts.add("Attachments");
        expectedTxts.add("EXECUTION_REPORT-cucumber.json");
        USE.chekHTMLReport(expectedTxts, start, end);
        //I dont exist => not run
        Assert.assertTrue(USE.findString("squash/IdontExist.feature", start) == -1);
        //idonthavefeatureextension.txt => error not feature extension DSL
        moveStartEnd();
        expectedTxts.clear();
        expectedTxts.add("squash/idonthavefeatureextension.txt");
        expectedTxts.add("status: error");
        expectedTxts.add("3: test creation: from compiled project to 'gherkin.java.cucumber' resource (gherkin.script)"); //DSL
        expectedTxts.add("details: 'squash/idonthavefeatureextension.txt' does not have '.feature' extension");
        expectedTxts.add("caused by: org.squashtest.ta.framework.exception.baddataexception:");//stacktrace
        USE.chekHTMLReport(expectedTxts, start, end);
        USE.chekNotExistInHTMLReport("Attachments", start, end);
        USE.chekNotExistInHTMLReport("EXECUTION_REPORT-cucumber.json", start, end);
        //T4 cucumber.api.PendingException
        moveStartEnd();
        expectedTxts.clear();
        expectedTxts.add("squash/t4_is_it_friday_3steps_3datasets_stepwhenundefineraiseexceptionko.feature");
        expectedTxts.add("status: error");
        expectedTxts.add("today is or is not friday - scenario outline -> dataset: 1/3");
        expectedTxts.add("org.squashtest.ta.framework.exception.instructionruntimeexception: cucumber.api.pendingexception: todo: implement me");
        Assert.assertTrue(USE.countOccurrencesOf("When Oops Java Method exists but is not implemented so raising exception", start, end) == 3);
        expectedTxts.add("Attachments");
        expectedTxts.add("EXECUTION_REPORT-cucumber.json");
        USE.chekHTMLReport(expectedTxts, start, end);
        //T5 1 secanrio + 1 scenario outline
        moveStartEnd();
        expectedTxts.clear();
        expectedTxts.add("squash/T5_is_it_friday_3steps_Exception_divideBy0.feature");
        expectedTxts.add("status: error");
        expectedTxts.add("Today is or is not Friday");
        expectedTxts.add("Then God Divide by zero Exception");
        expectedTxts.add("java.lang.ArithmeticException: / by zero");
        Assert.assertTrue(USE.countOccurrencesOf("org.squashtest.ta.framework.exception.InstructionRuntimeException: java.lang.ArithmeticException: / by zero", start, end) == 1);
        expectedTxts.add("Attachments");
        expectedTxts.add("EXECUTION_REPORT-cucumber.json");
        USE.chekHTMLReport(expectedTxts, start, end);
        // operations/SimpleOperations.feature failure sur 1 dataset
        moveStartEnd();
        expectedTxts.clear();
        expectedTxts.add("operations/SimpleOperations.feature");
        expectedTxts.add("Status: Failure");
        expectedTxts.add("Then sum should be 6");
        expectedTxts.add("java.lang.AssertionError: expected:<4> but was:<6>");
        Assert.assertTrue(USE.countOccurrencesOf("add numbers - Scenario Outline -> DataSet:", start, end) == 3);
        expectedTxts.add("multiply numbers");
        expectedTxts.add("Attachments");
        expectedTxts.add("EXECUTION_REPORT-cucumber.json");
        USE.chekHTMLReport(expectedTxts, start, end);
        //malformed feture => DSL
        moveStartEnd();
        expectedTxts.clear();
        expectedTxts.add("squash/MalformedFeature.feature");
        expectedTxts.add("Status: Error");
        expectedTxts.add("3: Test creation: FROM compiled project TO 'gherkin.java.cucumber' resource (gherkin.script)");
        expectedTxts.add("gherkin.ParserException$CompositeParserException: Parser errors");
        expectedTxts.add("5: Check if successful execution");
        USE.chekNotExistInHTMLReport("Attachments", start, end);
        USE.chekNotExistInHTMLReport("EXECUTION_REPORT-cucumber.json", start, end);
        USE.chekHTMLReport(expectedTxts, start, end);
    }

    @After
    public void clean() {
        USE.clean(projectBaseDirectory);
    }

    private void moveStartEnd() throws IOException {
        this.start = end;
        this.end = USE.findString(backFeatureList, end) + delta;
    }
}
