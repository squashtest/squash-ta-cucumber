/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin;

import java.io.File;
import org.apache.maven.plugin.MojoExecutionException;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;
import static org.junit.Assert.*;
import org.squashtest.ta.plugin.cucumber.library.Conf;

/**
 *
 * @author cjuillard
 */
public class BrowserTest {

    SquashTAGherkinTestRunableMojo mojo = new SquashTAGherkinTestRunableMojo();
    File projectPomLoc;
    File featureBase;
    SuiteSpecification suite;

    @Before
    public void set() {
        projectPomLoc = new File(System.getProperty("user.dir"));
        assertTrue(projectPomLoc.exists());
        assertTrue(projectPomLoc.isDirectory());
    }

    @Test
    public void ctrlBrowser() throws MojoExecutionException {
        mojo.projectBaseDirectory = projectPomLoc;
        TestWorkspaceBrowser browser = mojo.makeWorkspaceBrowser();
        assertNotNull(browser);
        File scrTestresources = new File(projectPomLoc, Conf.SRC_TEST_RESOURCES);
        assertTrue(scrTestresources.exists());
        assertTrue(scrTestresources.isDirectory());
        assertEquals(browser.getDefaultResourceFile().getAbsolutePath(), scrTestresources.getAbsolutePath());
    }
}
