/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.squashtest.ta.gherkin.exploitation.link.GherkinTestEcosystemModel;
import org.squashtest.ta.gherkin.exploitation.link.TestPointer;
import org.squashtest.ta.maven.AbstractBaseSquashTaMojo;
import org.squashtest.ta.maven.gherkin.json.listing.GherkinRunnerJsonTree;
import org.squashtest.ta.maven.gherkin.parser.GherkinTestSpecParser;

/**
 * Mojo to list all sceniaros in all feature files of the current test project
 * @author qtran
 */
@Mojo(
        name = "list",
        defaultPhase = LifecyclePhase.INTEGRATION_TEST,
        requiresProject = false
)
public class SquashTAGherkinListMojo extends AbstractBaseSquashTaMojo{

    @Parameter (
            property="project.build.directory"
    )
    private File buildDirectory;
    
    @Parameter (
            property="project.build.sourceEncoding"
    )
    private String encoding;
    
    @Override
    protected void execution() throws MojoExecutionException, MojoFailureException {
        getLogger().info("Initialization...");
        initLogging();
        init();
        resetOutputDirectory();
        File root = getBaseDir();
        String rootPath = root.getAbsolutePath();
        
        getLogger().info("Listing all .feature tests in the project: "+rootPath);
        GherkinTestSpecParser testSpecParser = new GherkinTestSpecParser(root, "**/*", encoding);
        
        List<TestPointer> testPointers = testSpecParser.parseTestList();
        getLogger().info("Found '" + testPointers.size() + "' test(s)");
        
        //create the json file
        Map<String, List<TestPointer>> ecosystemGroups = new GherkinTestEcosystemModel(testPointers, root).groupPointersByEcosystemDefinition();
        Map<String, List<TestPointer>> treeMap = new TreeMap<>(ecosystemGroups);
        new GherkinRunnerJsonTree(getOutputDirectory()).write(treeMap, false);
        
    }
    
    private void init() {
        if(getOutputDirectory() == null) {
            setOutputDirectory(new File(buildDirectory, "squashTF"));
        }
    }
    
}
