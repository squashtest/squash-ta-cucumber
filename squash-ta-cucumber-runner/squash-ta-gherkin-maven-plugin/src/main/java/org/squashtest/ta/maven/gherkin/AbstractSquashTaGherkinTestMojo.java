/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.init.EngineLoader;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.commons.factories.specification.TestSpecification;
import org.squashtest.ta.commons.json.listing.JsonTestTree;
import org.squashtest.ta.framework.exception.BadDataException;
import org.squashtest.ta.framework.facade.Configurer;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.gherkin.exploitation.factory.GherkinTMModeTestSuiteFactory;
import org.squashtest.ta.gherkin.exploitation.testworkspace.GherkinTestWorkspaceBrowser;
import org.squashtest.ta.gherkin.exporter.HtmlGherkinSuiteResultExporter;
import org.squashtest.ta.gherkin.result.GherkinSuiteResult;
import org.squashtest.ta.maven.AbstractSquashTaMojo;
import org.squashtest.ta.maven.param.json.JsonTestSuite;
import org.squashtest.ta.plugin.cucumber.json.CucumberJSONParser;
import org.squashtest.ta.plugin.cucumber.library.Conf;

/**
 * Abstract Class for Mojos checking or executing Gherkin Tests implemented in
 * java.
 *
 * @author cjuillard
 */
public abstract class AbstractSquashTaGherkinTestMojo extends AbstractSquashTaMojo {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSquashTaGherkinTestMojo.class);
    private static final String SRC = Conf.SEP + Conf.SRC;
    private static final String SRC_TEST_RESOURCES = Conf.SRC_TEST_RESOURCES;
    private static final String FEATURE_EXTENSION = Conf.FEATURE_EXTENTSION;
    private static final String TF_STORE = Conf.TF_TMP_TARGET;

    /**
     * @parameter general configuration.
     */
    protected File logConfiguration;

    /**
     * @parameter general configuration
     */
    protected File outputDirectory;
    
    /**
     * @parameter property="ta.debug.mode"
     */
    protected String debug;

    /**
     * @parameter expression="project.build.directory"
     */
    protected File buildDirectory;

    /**
     * @parameter property="ta.delete.json.file"
     */
    private boolean deleteJsonFile = false;

    /**
     * POM folder
     *
     * @parameter expression="basedir"
     */
    protected File projectBaseDirectory;

    /**
     * @parameter expression="project.groupId"
     */
    protected String projectGroupId;

    /**
     * @parameter expression="project.artifactId"
     */
    protected String projectArtifactId;

    /**
     * @parameter expression="project.version"
     */
    protected String projectVersion;

    /**
     * @parameter see configurers for SquashTA Mojo Defined in POM of executing
     * test project
     */
    protected Configurer[] gkconfigurers;

    /**
     * @parameter
     * see exporters for SquashTA Mojo Defined in POM of executing
     * test project
     */
    protected ResultExporter[] gkexporters;

    /**
     * @parameter expression="ta.feature" specific to gherkin tests. Either JSON
     * or a subfolder of src/test/resources or a feature file If null, all
     * feature files in the src/test/resources will be executed or checked
     */
    protected String featuresList;
    
     /**
     * @parameter expression="tf.feature" 

     * specific to gherkin tests. Either JSON
     * or a subfolder of src/test/resources or a feature file If null, all
     * feature files in the src/test/resources will be executed or checked
     */
    protected String featuresListTF;

    /**
     * @parameter Subfolder of src/test/resources dedicated to squashTM
     * generated features
     */
    protected String squashRoot;

    /**
     * @parameter property=project.compileClasspathElements
     */
    private List<String> compileCP;

    /**
     * @parameter property=project.testClasspathElements
     */
    private List<String> testCP;
    /**
     * @parameter property=project.build.sourceEncoding
     */
    private String encoding;

    /**
     * Compilation option
     */
    private List<String> compilationClasspthList = new ArrayList<>();

    private static final String TARGET = "target";

    private static final String SQUASHTF = "squashTF";

    /**
     * Defined type of Mojo executing (Test if the tests are runable or execute
     * the tests) set by Mojo.
     */
    protected boolean isDryRun = false;

    protected boolean addDryRunChecks = false;

    protected File featureFileDirectory = null;

    protected static final int MAX_DEEP_SEARCH = 5000;

    protected String testDirectory;

    protected SuiteSpecification specification = null;

    protected boolean isGherkinHTLMExporter;

    /**
     * set Configurers to parent (separate package)
     *
     * @param configurerTable
     */
    protected void setConfigurersToParent(Configurer[] configurerTable) {
        if (configurerTable != null) {
            setConfigurers(configurerTable);
        }
    }

    /**
     * set 'standard' exporters to parent (separate package) ie exporters using
     * DefaultSuiteResult
     *
     * @param exporters
     */
    protected void setExportersToParent(ResultExporter[] exporters) {
        for (ResultExporter exporter : exporters) {

            if (exporter instanceof HtmlGherkinSuiteResultExporter) {
                LOGGER.debug("SquashTA Gherkin Mojo: HtmlGherkinSuiteResultExporter configured..");
                HtmlGherkinSuiteResultExporter gherkinExporter = (HtmlGherkinSuiteResultExporter) exporter;
                gherkinExporter.setIsDryRun(isDryRun);
                isGherkinHTLMExporter = true;
            }
        }
        if (exporters != null) {
            super.setExporters(exporters);
        }
    }

    protected ResultExporter[] setStandardExportersToParentAndUpdategkExporters(ResultExporter[] exporters, boolean isDryRun) {
        if (exporters == null) {
            return null;
        }
        ResultExporter[] returned = null;
        List<ResultExporter> exporterForParents = new ArrayList<>();
        for (ResultExporter exporter : exporters) {
            if (exporter instanceof HtmlGherkinSuiteResultExporter) {
                LOGGER.debug("SquashTA Gherkin Mojo: HtmlGherkinSuiteResultExporter configured..");
                HtmlGherkinSuiteResultExporter gherkinExporter = (HtmlGherkinSuiteResultExporter) exporter;
                gherkinExporter.setIsDryRun(isDryRun);
                isGherkinHTLMExporter = true;
                returned = new ResultExporter[1];
                returned[0] = exporter;
            } else {
                exporterForParents.add(exporter);
            }
        }

        ResultExporter[] forParents = new ResultExporter[exporterForParents.size()];
        super.setExporters(exporterForParents.toArray(forParents));
        return returned;
    }

    @Override
    protected void exportResults(SuiteResult standardResults) {
        //call exportResult for exporter using DeafaultSuiteResult
        super.exportResults(standardResults);
        if (!isGherkinHTLMExporter) {
            return;
        }
        // change DefaultSuiteREsult -> GherkinSuiteResult
        if (encoding != null) {
            CucumberJSONParser.setEncoding(encoding);
        }
        SuiteResult results = new GherkinSuiteResult(standardResults, isDryRun, addDryRunChecks);
        //GO 
        Map<String, String> usedOutputDirectories = new HashMap<>();
        for (ResultExporter exporter : gkexporters) {
            try {
                exporter.setReportType(getReportType());
                String outputSubdirectory = exporter.getOutputDirectoryName();
                if (outputSubdirectory == null) {
                    outputSubdirectory = ".";
                }
                if (usedOutputDirectories.containsKey(outputSubdirectory)) {
                    getLogger().warn("Subdirectory '" + outputSubdirectory
                            + "' used by both " + exporter.getClass().getName()
                            + " and "
                            + usedOutputDirectories.get(outputSubdirectory));
                }
                usedOutputDirectories.put(outputSubdirectory, exporter
                        .getClass().getName());

                File reportTarget = new File(getOutputDirectory(), outputSubdirectory);
                reportTarget.mkdirs();
                exporter.write(reportTarget, results);
            } catch (IOException ex) {
                getLogger().error("Squash TF : a result exporter failed, some result data may be missing.", ex);
                cleanReportCucumber();
            }
        }
        cleanReportCucumber();
    }

    @Override
    public File getOutputDirectory() {
        return super.getOutputDirectory();
    }

    @Override
    public void setOutputDirectory(File outputDirectory
    ) {
        super.setOutputDirectory(outputDirectory);
        this.outputDirectory = outputDirectory;
    }

    protected void setFeaturesList(String featuresList) {
        this.featuresList = featuresList;
    }
    
    protected void setFeaturesListTF(String featuresListTF) {
        this.featuresListTF = featuresListTF;
    }


    protected void setBuildDirectory(File buildDirectory) {
        this.buildDirectory = buildDirectory;
    }

    protected void setProjectBaseDirectory(File projectBaseDirectory) {
        this.projectBaseDirectory = projectBaseDirectory;
    }

    @Override
    protected ReportType getReportType() {
        return ReportType.EXECUTION;
    }

    private void deleteJsonFile(File jsonFile) {
        if (deleteJsonFile) {
            jsonFile.delete();
        }
    }

    protected final TestWorkspaceBrowser makeWorkspaceBrowser() {
        GherkinTestWorkspaceBrowser browser = null;
        browser = new GherkinTestWorkspaceBrowser(new File(projectBaseDirectory.getParent()),
                new File(projectBaseDirectory, SRC_TEST_RESOURCES));
        return browser;
    }

    protected final void initDir() {
        super.setBaseDir(projectBaseDirectory);
        if (buildDirectory == null) {
            buildDirectory = new File(projectBaseDirectory, TARGET);
        }
        if (getOutputDirectory() == null) {
            setOutputDirectory(new File(buildDirectory, SQUASHTF));
        }
    }

    protected final SuiteSpecification convertoTestSuite(String featuresListIn) throws MojoExecutionException {

        if (featuresListIn != null && featuresListIn.trim().startsWith("{")) {
            convertoTestSuiteFromJson(featuresListIn);

            //add reserved subfolder for TM generated feature if necessary
            if (squashRoot != null) {
                convertTestSuiteFromSquashTMjson();
            }
        } else {
            convertoTestSuiteFromFeature(featuresListIn);
        }
        return specification;
    }
    
    private void convertoTestSuiteFromJson(String featuresListIn) throws MojoExecutionException {
        
        LOGGER.debug("SquashTA Gherkin Mojo: Parsing received JSON...");
        JsonTestSuite jsonTestsuite = new JsonTestSuite(null);
        
        /*
        * The pattern below will match testSuiteComposition with the format
        * : {file:pathToJsonFile}. When the testSuiteComposition has a such
        * format that means a file containing the json is provided and
        * located at pathToJsonFile
        */
        
        Pattern jsonFilePattern = Pattern.compile("^\\{file:(.*)\\}$");
        Matcher matcher = jsonFilePattern.matcher(featuresListIn.trim());
        if (matcher.matches()) {
            
            /*
            * When the testSuitComposition match, pathToJsonFile correspond
            * to the capturing group at index 1.
            */
            LOGGER.debug("format received {file:(.*)\\}");
            File jsonFile = new File(matcher.group(1));
            LOGGER.info("file to parse: " + jsonFile.getAbsolutePath());
            
            if (!jsonFile.exists()) {
                throw new BadDataException("Bad input for TestSuite, not existing file, received: " + featuresListIn);
            }
            
            specification = jsonTestsuite.parse(jsonFile);
            deleteJsonFile(jsonFile);
            
        } else {
            specification = jsonTestsuite.parse(featuresListIn);
        }
    }
    
    private void convertTestSuiteFromSquashTMjson() throws MojoExecutionException {
        
        File file = new File(featureFileDirectory, squashRoot);
        
        if (!file.exists()) {
            throw new MojoExecutionException("SquashTA Gherkin Mojo: Invalid value of parameter squashRoot: '" + squashRoot
                    + "'. squashRoot must be a subdirectory of src/test/resources");
        }
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("SquashTA Gherkin Mojo: add '" + squashRoot + "' to feature path from received JSON");
        }
        
        List<TestSpecification> newTestsList = new ArrayList<>();
        String subPath = squashRoot + "/";

        for (TestSpecification test : specification.getTests()) {
            newTestsList.add(new TestSpecification(subPath + test.getScript(), test.getId(), (Map) test.getParam()));
        }
        specification.setTests(newTestsList);       
    }
    
    private void convertoTestSuiteFromFeature(String featuresListIn) throws MojoExecutionException {
        
        LOGGER.debug("SquashTA Gherkin Mojo: Analysing  featuresList (ta.feature or tf.feature) received: ...");
        specification = new SuiteSpecification();
        LOGGER.debug("..featuresList: " + featuresListIn);

        List<String> tests = null;
        Path p = Paths.get(featuresListIn);
        File targetFile = null;
        
        if (!p.isAbsolute()) {
            LOGGER.debug("SquashTA Gherkin Mojo: featuresList is a relative path.");
            //relative file path: create a file with path combining the project base directory and user input path values!
            targetFile = new File(projectBaseDirectory, featuresListIn);
            
        } else {
            LOGGER.debug("SquashTA Gherkin Mojo: featuresList is an absolute path.");
            targetFile = new File(featuresListIn);
        }
        
        LOGGER.debug("..targetFile: " + targetFile);
        tests = treatAbsolutePath(targetFile);

        List<TestSpecification> testSpecList = new ArrayList<>();
        Integer index = 0;
        
        for (String test : tests) {
            TestSpecification testSpec = new TestSpecification(test, index.toString(), null);
            index++;
            testSpecList.add(testSpec);
        }
        specification.setTests(testSpecList);  
    }

    private List<String> treatAbsolutePath(File targetFile) throws MojoExecutionException {
        List<String> tests;
        if (targetFile.isDirectory()) {
            LOGGER.debug("SquashTA Gherkin Mojo: featuresList is a directory, searching feature files in it.");
            try {
                tests = buildExistingFeaturesList(targetFile.getCanonicalPath());
            } catch (IOException ex) {
                throw new RuntimeException("Abnormal condition, at that stage the file should be accessible", ex);
            }
        } else {
            LOGGER.debug("SquashTA Gherkin Mojo: featuresList is a single feature file.");
            tests = new ArrayList<>();
            Path featurePathBaseDirectory = featureFileDirectory.toPath();
            Path featurePath = targetFile.toPath();
            tests.add(featurePathBaseDirectory.relativize(featurePath).toString().replace("\\", "/"));
        }
        
        return tests;
    }
    
    /**
     * Build list of feature files in rootDirectory
     *
     * @param rootDirectory
     * @return
     * @throws org.apache.maven.plugin.MojoExecutionException
     */
    protected List<String> buildExistingFeaturesList(String rootDirectory) throws MojoExecutionException {

        Path featurePathBaseDirectory = featureFileDirectory.toPath();
        List<String> list = new ArrayList<>();
        Path start = Paths.get(rootDirectory);
        Stream<Path> stream = null;
        try {
            stream = Files.walk(start, 5000, FileVisitOption.FOLLOW_LINKS);
            list = stream.filter(Files::isRegularFile)
                    .filter(p -> p.getFileName().toString().endsWith(FEATURE_EXTENSION))
                    .filter(p -> p.toAbsolutePath().toString().startsWith(featureFileDirectory.getAbsolutePath()))
                    .map(p -> featurePathBaseDirectory.relativize(p).toString().replace("\\", "/"))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new MojoExecutionException("SquashTA Gherkin Mojo: Couldn't build featureList for directory: '" + rootDirectory + "'.", e);
        }
        return list;
    }

    protected void initExecute() throws MojoExecutionException {
        initDir();
        gkexporters = setStandardExportersToParentAndUpdategkExporters(gkexporters, isDryRun);
        setConfigurersToParent(gkconfigurers);
        featureFileDirectory = new File(projectBaseDirectory, SRC_TEST_RESOURCES);

        if (featuresListTF != null) {
            specification = convertoTestSuite(featuresListTF);

            if (featuresList != null) {
                LOGGER.warn("The parameters tf.feature and ta.feature are both declared. Only the tf.feature will be taken into account. tf.feature value: {} , "
                        + "ta.feature value (ignored): {}", featuresListTF, featuresList);
            }
        } else {
            if (featuresList != null) {
                specification = convertoTestSuite(featuresList);
            } else {
                // prevents a NullPointerException from reaching the user when no ta.feature or tf.feature is provided
                throw new MojoExecutionException("Missing ta.feature or tf.feature parameter. Please provide this parameter");
            }
        }
        
        resetOutputDirectory();
        JsonTestTree listing = new JsonTestTree(featureFileDirectory, getOutputDirectory());
        listing.generate(true);
        testDirectory = projectBaseDirectory.getName() + SRC;
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Searching for java files to compile in '" + testDirectory + "' folder");
        }
    }

    private void cleanReportCucumber() {
        File folderToRemove = new File(TF_STORE);
        if (folderToRemove.exists() && folderToRemove.isDirectory()) {
            org.apache.commons.io.FileUtils.deleteQuietly(folderToRemove);
        }
    }

    protected void addDependencies() {
        evaluateClasspath();
        if (compilationClasspthList.isEmpty()) {
            return;
        }
        URL[] classLoaderUrls = null;
        List<URL> urls = new ArrayList<>();

        File file = null;
        for (String sUrl : compilationClasspthList) {
            file = new File(sUrl);
            if (file.exists()) {
                try {
                    urls.add(file.toURI().toURL());
                } catch (MalformedURLException ex) {
                    LOGGER.warn("'" + sUrl + "' => unable to evaluate corresponding url.", ex);
                }
            } else {
                LOGGER.warn("'" + sUrl + "' => file does not exist");
            }
        }
        if (!urls.isEmpty()) {
            classLoaderUrls = (URL[]) urls.toArray(new URL[urls.size()]);
            URLClassLoader urlClassLoader = new URLClassLoader(classLoaderUrls, Thread.currentThread().getContextClassLoader());
            Thread.currentThread().setContextClassLoader(urlClassLoader);
        }
    }

    protected void evaluateClasspath() {
        Set unicityCP = new HashSet();
        if (testCP != null) {
            unicityCP.addAll(testCP);
        }
        if (compileCP != null) {
            unicityCP.addAll(compileCP);
        }
        if (!unicityCP.isEmpty()) {
            compilationClasspthList.addAll(unicityCP);
        }
    }

    public String getEncoding() {
        return encoding;
    }

    public SuiteResult generateDSLAndStartEngine(GherkinTMModeTestSuiteFactory factory) {
        if (encoding != null) {
            factory.setEncoding(encoding);
        }
        TestSuite suite = factory.generateTestSuite(projectGroupId, projectArtifactId, projectVersion, specification.getTests());
        TestWorkspaceBrowser browser = makeWorkspaceBrowser();

        addDependencies();

        EngineLoader loader = new EngineLoader();
        Engine engine = loader.load();

        applyConfigurers(engine);
        return engine.execute(suite, browser);
    }
    
 @Override
    protected void ifTestsFailedThenFail(SuiteResult results) throws MojoFailureException{
		GeneralStatus status = results.getStatus();                       
		if (statusFailsMojo(status) ){			               
			throw new MojoFailureException(this, " Tests failed / crashed", buildFailedMessage(results));
		}
	}
}
