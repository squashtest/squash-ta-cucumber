/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin.json.listing;

import java.io.File;
import java.util.List;
import java.util.Map;
import org.squashtest.ta.gherkin.exploitation.link.TestPointer;
import org.squashtest.ta.squash.ta.addon.logic.kit.AbstractRunnerJsonTree;

/**
 * Implementation of the gherkin test with its scenarios to JSON transformation
 * that creates the JSON test list used by Squash TM
 * 
 * @author qtran
 */
public class GherkinRunnerJsonTree extends AbstractRunnerJsonTree<TestPointer> {
    
    public GherkinRunnerJsonTree(File destinationDir) {
        super(destinationDir);
    }

    @Override
    protected String getTestPointerTestName(TestPointer testPointer) {
        return testPointer.getScenario().getName();        
    }

    @Override
    protected Map<String, List<String>> getMetadataForTestPointer(TestPointer testPointer) {
        // Unimplemented in the first - metadataless - version of the Cucumber Runner.
        // We'll have to implement this if we DO get to provide metadata in the runner.
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
