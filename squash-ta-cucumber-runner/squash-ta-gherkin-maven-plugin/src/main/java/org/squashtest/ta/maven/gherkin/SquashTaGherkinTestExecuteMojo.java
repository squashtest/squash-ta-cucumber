/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.gherkin.exploitation.factory.GherkinTMModeTestSuiteFactory;

/**
 * Mojo to execute feature files.
 *
 * @author
 *
 * @goal run
 * @requiresDependencyResolution test
 * @phase integration-test
 *
 */
public class SquashTaGherkinTestExecuteMojo extends AbstractSquashTaGherkinTestMojo {

    private static final Logger LOGGER = LoggerFactory.getLogger(SquashTaGherkinTestExecuteMojo.class);

    @Override
    protected SuiteResult executeImpl() throws MojoExecutionException,
            MojoFailureException {

        LOGGER.info("SquashTaGherkinTestExecuteMojo (Maven Mojo to execute feature files) starting ...");

        initExecute();

        GherkinTMModeTestSuiteFactory factory = new GherkinTMModeTestSuiteFactory(testDirectory, false, false, isGherkinHTLMExporter);
        return generateDSLAndStartEngine(factory);
    }
}
