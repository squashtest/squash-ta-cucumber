/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.maven.gherkin.parser;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.maven.plugin.MojoFailureException;
import org.squashtest.ta.gherkin.exploitation.explorer.GherkinTestExplorer;
import org.squashtest.ta.gherkin.exploitation.explorer.GherkinTestSuite;
import org.squashtest.ta.gherkin.exploitation.link.TestPointer;
import org.squashtest.ta.maven.param.json.JsonTestSuite;
import org.squashtest.ta.squash.ta.addon.logic.kit.TestSpecParserBase;

/**
 *
 * @author qtran
 */
public class GherkinTestSpecParser extends TestSpecParserBase<TestPointer>{
    
    private final File projectBase;
    
    private final String encoding;
    
    private final GherkinTestExplorer explorer;
    
    private List<GherkinTestSuite> testSuites;
    
    public GherkinTestSpecParser(File projectBase, String testList, String encoding) throws MojoFailureException {
        super(testList);
        try {
            this.projectBase = projectBase.getCanonicalFile();
            this.encoding = encoding;
        } catch (IOException ex) {
            throw new MojoFailureException("Failed to normalize project base path", ex);
        }
        
        this.explorer = new GherkinTestExplorer();
    }

    @Override
    protected JsonTestSuite createJsonTestSuite() {
        return new JsonTestSuite(projectBase);
    }

    @Override
    protected void addAllTestsWildCard(List<TestPointer> list) {
        try {
            List<GherkinTestSuite> currentTestSuites = getTestSuites();

            for (GherkinTestSuite testSuite : currentTestSuites) {
                String featureName = testSuite.getFeatureName();
                File suiteFile = computeRelativeSuiteFile(testSuite);
                testSuite.getTests().forEach(scenario -> 
                        list.add(new TestPointer(suiteFile, featureName, scenario.getScenario()))
                );
                
            }
        } catch (IOException ex) {
            throw new RuntimeException("Failed to extract Gherkin test file", ex);
        }
    }

    @Override
    protected void addIndividualTest(String string, List<TestPointer> list) throws MojoFailureException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private List<GherkinTestSuite> getTestSuites() {
        synchronized(this){
            if (testSuites == null) {
                testSuites = explorer.getTestSuites(projectBase, encoding);
            }
            return testSuites;
        }
    }

    private File computeRelativeSuiteFile(GherkinTestSuite testSuite) throws IOException {
        final String suiteNormalizedPath = testSuite.getSourceFile().getCanonicalPath();
        final int projectNormalizedPathLength = projectBase.getPath().length();
        String suiteRelativePath=suiteNormalizedPath.substring(projectNormalizedPathLength+1);
        return new File(suiteRelativePath);
    }
    
}
