..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##############################
Squash TF Cucumber Java Runner
##############################


********
Overview
********

The **Squash Test Factory (Squash TF) Cucumber Java Runner**\  allows you to run your tests written in Gherkin language with the TF ecosystem. The Java implementation of your Gherkin scripts must just be compatible with the cucumber framework.

The runner (Maven based) enables to run your scripts (a selection of feature files) from an IDE, command line, TF Server but also from **Squash T**\ est **M**\ anagement (**Squash TM**\ ). 

As with TF framework, different reporting are available. But with Cucumber Java Runner you can also have an HTML report specific to Gherkin scripts with among other informations, the result of the execution at each level (suite, feature, scenario, dataset, step), as well as the step by step feature display with the matching java methods executed. 

In the case where the execution launcher is Squash Test Management (Squash TM), and simply by configuring TF, the runtime test status and the final report can be automatically forward to Squash TM.

The runner allows either to run the features either to check if the features are runnable (dryrun) , ie if the automation work was done which means that a java implementation of each step exists, is not empty and not reduce to throw an 'cucumber.api.PendingException'.

********
Contents
********

.. toctree::
   :maxdepth: 2

   Squash TF Doc Homepage <https://squash-tf.readthedocs.io>
   Usage <usage.rst>
