..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


The purpose of this section is to describe how to create a test project from scratch or how to configure an existing Gherkin/Java project so it can be launched by the TF Runner. This includes of course the case where feature files have been written with squashTM and extract from a GIT repository.
 
.. _local-environment-configuration:
 
.. note::
   Please, check your Maven and JDK installations first as explains `here <./../../runner/overview.html>`_

------------
Java project
------------

.. _From scratch:

> From scratch
**************

In the case you are starting from scratch, it is recommended to create the test project with the squash-ta-cucumber-archetype. 
Before typing the line below in a terminal, please update the version of the archetype if necessary. 

.. code-block:: shell

  mvn archetype:generate -DarchetypeGroupId=org.squashtest.ta.galaxia -DarchetypeArtifactId=squash-ta-cucumber-archetype -DarchetypeVersion=1.1.0-RELEASE -Dpackage=jar -Dversion=1.0.0-SNAPSHOT -DinteractiveMode=true

The version of the archetype is 1.1.0-RELEASE in this sample,

.. image:: ./_static/images/archetype.jpg
   :align: center

and you can find all available versions in our repository: http://repo.squashtest.org/maven2/release/archetype-catalog.xml 

After typing the command line on top and supplying a groupId and an artifactId ('org.squashtest.galaxia' and 'gherkin_sample' in the example above), you should have 'BUILD SUCCESS'.

.. image:: ./_static/images/archetype_result.jpg
   :align: center
 
If not, please read the note about :ref:`environment-configuration<local-environment-configuration>`. 
The most common causes of errors are: Maven is not installed or not in path, the access to our repository are not declared in the Maven's setting.xml file, the TF archetype version does not exist in our repository.    

Now you just have to open the newly created project in an IDE and start writing your tests. 
With Eclipse:

.. image:: ./_static/images/eclipse_import.jpg
   :align: center

   
Note that an example of a feature with its java code is provided with the archetype.

.. image:: ./_static/images/sample_eclipse2.jpg
   :align: center

See  :ref:`here<Execute>` for how to run it. 

See  :ref:`here<Implementation>` for how to implement your own tests.

See  :ref:`here<POM_options>` for how to change TF runner's default options.

.. _Existing project:

> Existing project
******************

In the case you already have a test project gherkin/cucumber and want to run it with TF, add or modify these 4 blocks to your pom.xml file (located at the root of your project) as shown hereafter. 

.. image:: ./_static/images/pom_compact3.jpg
   :align: center

Add the property for runner's version (and the sources's encoding if necessary) like this

.. code-block:: shell

	<properties>
		<!-- Squash-TF-cucumber runner version used by the project -->		
		<ta.cucumber.runner.version>1.1.0-RELEASE</ta.cucumber.runner.version>
		<!-- optional source encoding -->	
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

Add this <plugin> block above in your build/plugins block. 

.. code-block:: shell

	<build>
		<plugins>
			<!-- Configuration of the Squash TA framework used by the project -->
			<plugin>
				<groupId>org.squashtest.ta.galaxia</groupId>
				<artifactId>squash-tf-gherkin-maven-plugin</artifactId>
				<version>${ta.cucumber.runner.version}</version>
				<configuration>
					<featuresList>${ta.feature}</featuresList>
					<squashRoot>squash</squashRoot>
					<!-- DryRunOptions for dryrun goal only -->
					<additionnalDryRunChecks>true</additionnalDryRunChecks>
					
					<!-- Define exporters -->
					<gkexporters>
						<exporter
							implementation="org.squashtest.ta.commons.exporter.surefire.SurefireSuiteResultExporter">
							<jenkinsAttachmentMode>${ta.jenkins.attachment.mode}</jenkinsAttachmentMode>
						</exporter>
						
						<exporter
							implementation="org.squashtest.ta.gherkin.exporter.HtmlGherkinSuiteResultExporter" />
							
					</gkexporters>
					
					<!-- Define configurers -->
					<gkconfigurers>
						<configurer implementation="org.squashtest.ta.maven.TmCallBack">
							<!-- <tmCallBack> -->
							<endpointURL>${status.update.events.url}</endpointURL>						
							<executionExternalId>${squash.ta.external.id}</executionExternalId>
							<jobName>${jobname}</jobName>
							<hostName>${hostname}</hostName>
							<endpointLoginConfFile>${squash.ta.conf.file}</endpointLoginConfFile>
							<reportBaseUrl>${ta.tmcallback.reportbaseurl}</reportBaseUrl>
							<jobExecutionId>${ta.tmcallback.jobexecutionid}</jobExecutionId>
							<reportName>${ta.tmcallback.reportname}</reportName>
							<!-- </tmCallBack> -->
						</configurer>
					</gkconfigurers>
				</configuration>
				
				<executions>
					<execution>
						<goals>
						<!-- to execute feature files -->
							<goal>run</goal>
							<!-- to check feature files are runable (DryRun) -->
							<goal>dryrun</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	
Add the this block for the path to our repository:

.. code-block:: shell	

	<!-- Squash TA maven plugin repository -->
		<pluginRepositories>
			<pluginRepository>
			<id>org.squashtest.plugins.release</id>
			<name>squashtest.org</name>
			<url>http://repo.squashtest.org/maven2/releases</url>
			<snapshots>
			<enabled>false</enabled>
			</snapshots>
			<releases>
			<enabled>true</enabled>
			</releases>
			</pluginRepository>
		</pluginRepositories>
   
Add these dependencies:

.. code-block:: shell	

		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-java</artifactId>
			<version>4.0.0</version>
		</dependency>

		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-junit</artifactId>
			<version>4.0.0</version>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
		</dependency>
		
		
Except for the TF-TM link parameters and eventually the runner's version, you can use all default settings. See :ref:`Runner's options<POM_options>` to 
modify them.

.. _Implementation:

------------------
Scripting / Coding
------------------

> Overview
**********

.. note::
   Your project should be based on the Cucumber framework. Your tests written in Gherkin language should be implemented in java language.
   
We recommend that you structure your project as follows:

* the feature files should be in src/test/resources (or any subdirectory)

* the Java implementation in src/test/java

Below the very simple example of a feature and its implementation provided with the TF archetype.

.. image:: ./_static/images/Gherkin_sampleProjectEclipse2.jpg
   :align: center

See:

* https://docs.cucumber.io/gherkin/reference/ for `Gherkin Language Reference`
 
* https://docs.cucumber.io/cucumber/step-definitions/ for how to write your `Step Definitions`, ie the java methods that link them to steps of your Gherkin script.   

> Advices
*********

.. _Encoding_scripting:

About the encoding and the language used:

*  If you use the TF artifact to create your project, the encoding is set to UTF-8 (java/feature).  Of course, you can modify it through the property 'project.build.sourceEncoding' in the POM.xml file. 

*  If you declare an encoding in your feature files (#encoding: utf-8 by example), make sure there is no inconsistency with what is declared in your POM.xml file.
  
* If your feature files are written with UTF-8 (or any special encoding), a good practice is to name your java methods only with ASCII characters. 

.. image:: ./_static/images/GherkinKeywordEnFrancais_BestPractice.jpg
   :align: center

* You can of course use keywords in a given language by declaring it in your feature files (check only the consistency of implied encoding)

.. image:: ./_static/images/featureKW_fr.jpg
   :align: center

About the glue:   

All the classes of the project will be used to find the methods matching with a step of the features.  
There should not be multiple implementations for the same step. That is, each annotation that identifies a step must be on a single java method (even if the method with the duplicate annotation is in a different class or classpath).

.. image:: ./_static/images/duplicate2.jpg
   :align: center

.. _Execute:

|

--------------
Listing (list)
--------------

This Mojo enables one to list as a Json file the available implemented tests.
In order to do so one only needs to run the following command :

.. code-block:: shell

  mvn squash-tf-gherkin:list

The command is structured as follows

* ``mvn`` : launch Maven
* ``squash-tf-gherkin:list``  :
  the actual listing Mojo provided by the |squashTF| Java Gherkin Runner.
  It lists all Gherkin tests that can be discovered in the test project.

If the build is successful, a generated report (JSON file) named testTree.json is created at **target/squashTF/test-tree**.
It is a simple JavaScript table listing available test grouped by "ecosystems".

.. code-block:: json

    {
		"timestamp": "2019-10-25T12:37:01.259+0000",
		"name": "tests",
		"contents": [{
				"name": "src/test/resources/squash/good-example.feature",
				"contents": [{
						"name": "What to do when concombre is broken",
						"contents": null
					}, {
						"name": "What to do when concombre is broken again",
						"contents": null
					}
				]
			}, {
				"name": "src/test/resources/with-empty-scenario.feature",
				"contents": [{
						"name": "",
						"contents": null
					}, {
						"name": "What to do when concombre is broken again",
						"contents": null
					}
				]
			}, {
				"name": "typical.feature",
				"contents": [{
						"name": "What to do when concombre is broken",
						"contents": null
					}, {
						"name": "What to do when concombre is broken again",
						"contents": null
					}
				]
			}
		]
	}

|

---------------------
Execute (run, dryrun)
---------------------

.. note::
	Please, if you want to start feature files execution from TM, refer to the Squash TM corresponding documentation.
	If you want to start execution from Squash TF execution Server, use the template involved.

> Command line
**************
	
To start the execution of all the tests (feature files) of the project from a terminal, go to the directory of your project (where the POM.xml file is located) and use one the following commands by replacing /path/to/project with the valid path:

* for the goal "run":

.. code-block:: shell

  mvn squash-tf-gherkin:run -Dta.feature=/path/to/project/src/test/resources 

* for the goal "dryrun":


.. code-block:: shell

  mvn squash-tf-gherkin:dryrun -Dta.feature=/path/to/project/src/test/resources 
  
With our sample and for the goal dryrun

.. image:: ./_static/images/dryrunCLI2.jpg
   :align: center


See :ref:`Selecting tests<ta.feature>` for how to select the tests to execute or :ref:`Expected_Result` 

> Eclipse launcher
******************

If you use Eclipse, you can create specific launch configurations (one to run and one to dryrun).
Select the menu "run" then "run Configuration..." and create a launch like this for a run goal:

.. image:: ./_static/images/M2Configuration2.jpg
   :align: center  
  
Check your Maven and JDK (JRE tab) configurations.

Then, select a feature file or a subdirectory of src/test/resources and click run / Run gherkin selected test

.. image:: ./_static/images/RunFromEclipseLaunch.jpg
   :align: center  
   
.. _POM_options:

|

----------------------
Runner's options (POM)
----------------------

Some options or parameters can be configured with the pom.xml file of your test project.

.. _Runner_version:

> Runner's version
******************

.. code-block:: shell

	<properties>
		<!-- Squash-TF-cucumber runner version used by the project -->		
		<ta.cucumber.runner.version>1.1.0-RELEASE</ta.cucumber.runner.version>
		...
	</properties>
		
See http://repo.squashtest.org/maven2/release/org/squashtest/ta/galaxia/squash-ta-cucumber/ for all available versions.

> Encoding
**********

.. code-block:: shell

	<properties>
		...
		<!-- optional: source encoding -->	
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	
Before changing the encoding, please read  :ref:`About encoding<Encoding_scripting>`

.. _ta.feature:

> Selecting tests
*****************

.. code-block:: shell

		<featuresList>${ta.feature}</featuresList>
		
You can choose the features to be processed by changing the ta.feature parameter above for both goals. The supported values are:

*   /path/to/project/src/test/resources/**../subfolder**\   => for all feature files in the given subfolder of src/test/resources.
*   /path/to/project/src/test/resources/**../myTest.feature**\   => for a single feature.
*   {file:path/to/JSONfile/MyFeatureList.json} => for a file containing a list of feature files.
    The contents of the json file should look like this example:

.. code-block:: json

    {
      "test": [
        {
          "id": "",
          "script": "pathRelativeToSquashRoot/TEST1.feature",
          "param": ""
        },
        {
          "id": "",
          "script": "pathRelativeToSquashRoot/TEST2.feature",
          "param": ""
        }
      ]
    }

where the given path for the feature should be relative to the src/test/resources/squash folder. You can change this relative path by changing the <squashRoot> value in the project POM file.
	
*   a json containing a list of feature files like:

.. code-block:: json

    {
      "test": [
        {
          "script": "pathRelativeToSrcTestResources/test1.feature"
        },
        {
          "script": "pathRelativeToSrcTestResources/test2.feature"
        }
      ]
    }

Warning: the Gherkin files should have a ".feature" extension otherwise they will be ignored.

.. _squashRoot:

> TM squashRoot
***************

.. code-block:: shell

	<squashRoot>squash</squashRoot> 

This parameter specifies a subpath for the feature files when the tests suite execution is requested by SquashTM.
If you do not start running from Squash TM, you can change the value of squashRoot or disable it by commenting the line. But if you start running from TM, do not change this parameter otherwise all the feature files will be "NOT_FOUND".

The value of this tag point out the root folder where TF runner should look for the gherkin files to run. It is taken into account only if the parameter "ta.feature" is a JSON file (ie ignored otherwise).
The value should be a directory and its relative path from src/test/resources folder.
For example and for the JSON above, the runner will look for the file TEST1.feature in /pathToProject/src/test/resources/squash/pathRelativeToSquashRoot.


.. _additionnalDryRunChecks:

> Additional DryRun Checks
***************************

.. code-block:: shell

	<additionnalDryRunChecks>true</additionnalDryRunChecks> 

Boolean used only if the goal is 'dryrun', ignored otherwise. 
If activate (true), the TF Gherkin plugin  will help you improve the 'dryrun' diagnosis of Cucumber by a search:

*  for empty methods
*  for methods reduce to the skeleton cucumber: "throw new cucumber.api.PendingException();"

.. _gkconfigurers_gkexporters:

> Reports selection
*******************

The same reports as for a TA scripts are available (Html, Junit, Surfire). You can request them by configuring the corresponding exporters.
Refer to TA documentation for these topics, only replace the <exporters> tag's name for a SquashTA project with <gkexporters> and add exporter(s) as you want.

If you used our artifact, the specific HTML reporting for the Gherkin tests suite is configured by default as follow : 

.. code-block:: shell

		<gkexporters>
			<exporter implementation="org.squashtest.ta.commons.exporter.surefire.SurefireSuiteResultExporter">
				<jenkinsAttachmentMode>${ta.jenkins.attachment.mode}</jenkinsAttachmentMode>
			</exporter>

			<exporter implementation="org.squashtest.ta.gherkin.exporter.HtmlGherkinSuiteResultExporter" />
		</gkexporters>

Note that you can only have one HTML reporting, either SquashTA ("org.squashtest.ta.commons.exporter.html.HtmlSuiteResultExporter") or Cucumber's runner ("org.squashtest.ta.gherkin.exporter.HtmlGherkinSuiteResultExporter") 
 

> TF-TM link
************

  The <gkconfigurers> block of the POM.xml file contains the items for the configuration of the Squash TF Server and squash TM link. It is identical to the <configurers> blocks of other TF projects (same subfields). Refer to TF documentation for this topic.

|

.. _Expected_Result:

----------------
Expected results
----------------

> overview
**********

All TF reports are in the **target/squashTF**\  subfolder from your java project. We recommend you the most complete reporting for the Gherkin Suite Tests namely to use the TF HTML Gherkin exporter. If you did not create the project with the squash-ta-cucumber-archetype, check you have configured it (line with 'HtmlGherkinSuiteResultExporter' in the POM.xml file).

This reporting, the file **target/squashTF/html-reports/squash-tf-report.html**\  starts like this:

.. image:: ./_static/images/summary2.jpg
   :align: center

The status of the test suite gives you a summary of the execution of each feature files of the suite (framed in red above).   

Except if feature file is NOT_RUN or NOT_FOUND, you can watch for details for each feature file in corresponding dedicated HTML block.

Each block representing a feature contains a sub-block for each scenario.

For a run goal each execution of a outline scenario with a dataset is displayed  as a scenario.
For a dryrun goal, each scenario outline is displayed only once.

.. image:: ./_static/images/SimpleOperations_run_dryrun.jpg
   :align: center
   
If a result of a scenario is failure or error the following scenarios are executed.
For the run goal, the step following the step in error are obviously not run, but for the dryrun goal all the steps are always checked.

Please note that if there is a technical or configuration error by example during compilation or during parsing a malformed feature file, technical informations will be displayed instead of the feature file.

> Definition of Status
**********************

The table below gives you the TF status of the feature file/scenario/step for different cases of error. 
In the case where a feature file contains several scenarios and/or outline scenarios (same scenario with several datasets), the status of the feature is the result is the compilation of any intermediate results. 



+----------------------------------------+-----------------+ 
|                                        |**Test Status**\ | 
+========================================+=================+            
|**Ecosystem's setup:**\                 | *...*           |     
|                                        |                 |
|    no compiling project                |  NOT_RUN        |
|                                        |                 |   
|    existing duplicate implementation   | (for all tests) |
|                                        |                 |        
+----------------------------------------+-----------------+ 
|**Feature loading:**\                   |  *...*          |                
|                                        |                 |    
|   Feature file not found               |  NOT_FOUND      |
|                                        |                 |
|   Feature file not ending by .feature  |  ERROR          | 
|                                        |                 |    		
|   Malformed feature (not parsable file)|  ERROR          | 
|                                        |                 |    
+----------------------------------------+-----------------+
|**Executing goal 'run':**\              |  *...*          | 
|                                        |                 |
|   Step not implemented (no matching)   |  ERROR          | 
|                                        |                 |
|   Runtime error (/0, nullPOinter,..)   |  ERROR          |
|                                        |                 |    
|   Cucumber.runtime.* exception         |  ERROR          |
|                                        |                 |   
|   JUNIT assert                         |  FAILURE        |
|                                        |                 |   
+----------------------------------------+-----------------+	
|**Executing goal 'dryrun':**\           |  *...*          | 
|                                        |                 |
|   Step not implemented (no matching)   |  FAILURE        | 
|                                        |                 |
|   Step matching with empty java method |  FAILURE (1)    |
|                                        |                 |    
|   cucumber.api.PendingException()      |  FAILURE (1)    |
|                                        |                 |    
+----------------------------------------+-----------------+		

(1): only if <additionnalDryRunChecks> is set to true, else status is SUCCESS.



